import { readFileSync } from 'node:fs'

const config = JSON.parse(readFileSync("/etc/dynip/config.json").toString('utf-8'));

const headers = {
  "X-Auth-Email": config.userId,
  "X-Auth-Key": config.apiToken,
  "Content-Type": "application/json"
};

const prefix_url = `https://api.cloudflare.com/client/v4/zones/${config.zoneId}`;


const recs: { id: string; ip: string }[] = [];

const getIds = async () => {
  while (recs.length != 0) recs.pop();
  for (const el of config.hosts) {
    const response = await fetch(`${prefix_url}/dns_records?name=${el}`, {
      headers: headers
    });
    if (!response.ok) {
      throw { err: "failed to fetch ids", code: response.status, json: await response.json() };
    }
    const json = await response.json()
    if (json.result.length === 0) {
      console.warn(
        `Got no result while searching for ${el}. Check if the DNS entry exists.`,
      );
      continue;
    }
    const toPush = { id: json.result[0].id, ip: json.result[0].content };
    recs.push(toPush);
  }
  return recs;
};

const handleChange = async (
  el: { id: string; ip: string },
  index: number,
  ip: string,
) => {
  const response: any = await fetch(`${prefix_url}/dns_records/${el.id}`, {
    method: "patch",
    body: JSON.stringify({ content: `${ip}` }),
    headers: headers
  });
  if (response.ok) {
    console.log(`Updated DNS content from ${el.ip} to ${ip}`);
    recs[index].ip = ip;
  } else {
    throw { err: "failed to update ip", code: response.code, json: await response.json() };
  }
};

// should not throw
const check = async () => {
  try {
    const response = await fetch("https://api64.ipify.org?format=json");
    if (!response.ok) {
      throw { err: "Bad Code while checking ip", code: response.status, json: await response.json() };
    }
    const ip = await response.json();
    for (const [index, value] of recs.entries()) {
      if (value.ip === ip.ip) continue;
      await handleChange(value, index, ip.ip);
    }
  } catch (e) {
    console.error(e);
  }
  setTimeout(check, 60000);
};


const main = async () => {
  try {
    // because js sucks i am allowed to do this, in any sane language this would be illegal
    recs.length = 0;
    await getIds();
    check();
  } catch (e: any) {
    setTimeout(main, 1000);
  }
};


main().then();
