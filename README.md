## Description  
DynIP is a very simple dynamic IP solution for cloudflare users.  
  
## How To Use  
Edit the config file at /etc/dynip/config.json with your api key and hosts.  
A [systemd script](dynip.service) is included which can be used to automatically run the binary.  

## Building and Installaion  
* For Building run   
```  
make all
```  
* For installation 

```  
make install  
```  

`make install` installs the dynip binary, the sample configuration file and the systemd service at the correct locations.
