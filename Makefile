NAME = crestfalln/dynip
VERSION = 0.2.0

ifneq (,$(wildcard /etc/dynip/config.json))
	CFG_PATH = /etc/dynip/config-new.json
else
	CFG_PATH = /etc/dynip/config.json
endif

install: build dynip config systemd 
all: build

build:
	mkdir -p bin
	bun build --compile ./updater.ts --outfile=bin/dynip.ts

dynip:
	systemctl stop dynip
	mkdir -p /opt/dynip
	cp bin/dynip.ts /opt/dynip/ 
	chown root:root /opt/dynip/dynip.ts
	chmod 755 /opt/dynip/dynip.ts

config: 
	mkdir -p /etc/dynip/
	cp config.json $(CFG_PATH)
	chown root:root $(CFG_PATH)
	chmod 644 $(CFG_PATH)

systemd:
	cp dynip.service /etc/systemd/system/dynip.service
	chown root:root /etc/systemd/system/dynip.service
	chmod 644 /etc/systemd/system/dynip.service
	systemctl daemon-reload
